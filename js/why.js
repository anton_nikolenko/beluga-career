$(document).ready(function(){
  $(window).scroll(function() {
    const $whyBlock = $('.js-why-block');

    const $whyOverlay = $('.js-why-overlay');

    const top_of_element = $whyBlock.offset().top;
    const bottom_of_element = $whyBlock.offset().top + $whyBlock.outerHeight();
    const bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
    const top_of_screen = $(window).scrollTop();

    const visOffsetStart = 300;

    if ((bottom_of_screen > top_of_element + visOffsetStart) && (top_of_screen < bottom_of_element)) {
      $whyBlock.addClass('is-visible')
      $whyOverlay.addClass('is-visible')
    } else {
      $whyBlock.removeClass('is-visible')
      $whyOverlay.removeClass('is-visible')
    }

  });


});
