const gallery_content = '<div class="js-gallery js-gallery-1 gallery">\n' +
  '    <div class="js-gallery-close gallery__close" data-gallery="1">\n' +
  '      <svg viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
  '        <path fill-rule="evenodd" clip-rule="evenodd" d="M1.43447 1.15138C1.74689 0.838959 2.25342 0.838958 2.56583 1.15138L20.8487 19.4342C21.1611 19.7466 21.1611 20.2532 20.8487 20.5656L20.5658 20.8484C20.2534 21.1609 19.7469 21.1609 19.4345 20.8484L1.15162 2.56559C0.839204 2.25317 0.839203 1.74664 1.15162 1.43422L1.43447 1.15138Z" fill="white"/>\n' +
  '        <path fill-rule="evenodd" clip-rule="evenodd" d="M20.8487 1.43422C21.1611 1.74664 21.1611 2.25317 20.8487 2.56559L2.56584 20.8484C2.25342 21.1609 1.74689 21.1609 1.43447 20.8484L1.15162 20.5656C0.839204 20.2532 0.839202 19.7466 1.15162 19.4342L19.4345 1.15138C19.7469 0.838959 20.2534 0.838959 20.5658 1.15138L20.8487 1.43422Z" fill="white"/>\n' +
  '      </svg>\n' +
  '    </div>\n' +
  '\n' +
  '    <div class="js-gallery-photos gallery__photo">\n' +
  '      <img class="is-visible js-gallery-photo js-gallery-photo-1" src="img/gallery/1/1.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-2" src="img/gallery/1/2.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-3" src="img/gallery/1/3.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-4" src="img/gallery/1/4.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-5" src="img/gallery/1/5.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-6" src="img/gallery/1/6.jpg" alt="">\n' +
  '    </div>\n' +
  '\n' +
  '    <div class="js-gallery-prev gallery__prev img-w">\n' +
  '      <svg viewBox="0 0 32 12" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
  '        <path fill-rule="evenodd" clip-rule="evenodd" d="M6.77187 0.684955C6.83602 0.49252 6.60599 0.337848 6.45198 0.469858L0.354527 5.69624C0.168283 5.85588 0.168282 6.14401 0.354527 6.30365L6.45198 11.53C6.60599 11.662 6.83602 11.5074 6.77187 11.3149L5.16689 6.5H31.6C31.8209 6.5 32 6.32091 32 6.1V5.9C32 5.67909 31.8209 5.5 31.6 5.5H5.16686L6.77187 0.684955Z" fill="white"/>\n' +
  '      </svg>\n' +
  '    </div>\n' +
  '\n' +
  '    <div class="js-gallery-next gallery__next img-w">\n' +
  '      <svg viewBox="0 0 32 12" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
  '        <path fill-rule="evenodd" clip-rule="evenodd" d="M25.2288 11.3149C25.1647 11.5074 25.3947 11.662 25.5487 11.53L31.6461 6.30365C31.8324 6.14401 31.8324 5.85588 31.6461 5.69624L25.5487 0.469858C25.3947 0.337848 25.1647 0.49252 25.2288 0.684955L26.8338 5.5H0.4C0.179086 5.5 0 5.67909 0 5.9V6.1C0 6.32091 0.179086 6.5 0.4 6.5H26.8338L25.2288 11.3149Z" fill="white"/>\n' +
  '      </svg>\n' +
  '    </div>\n' +
  '\n' +
  '    <div class="gallery__info">\n' +
  '      <div class="gallery__title">Наша команда</div>\n' +
  '      <div class="gallery__pagination">\n' +
  '        <span class="js-gallery-current">1</span> / <span class="js-gallery-amount">6</span>\n' +
  '      </div>\n' +
  '    </div>\n' +
  '  </div>\n' +
  '\n' +
  '  <div class="js-gallery js-gallery-2 gallery">\n' +
  '    <div class="js-gallery-close gallery__close" data-gallery="2">\n' +
  '      <svg viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
  '        <path fill-rule="evenodd" clip-rule="evenodd" d="M1.43447 1.15138C1.74689 0.838959 2.25342 0.838958 2.56583 1.15138L20.8487 19.4342C21.1611 19.7466 21.1611 20.2532 20.8487 20.5656L20.5658 20.8484C20.2534 21.1609 19.7469 21.1609 19.4345 20.8484L1.15162 2.56559C0.839204 2.25317 0.839203 1.74664 1.15162 1.43422L1.43447 1.15138Z" fill="white"/>\n' +
  '        <path fill-rule="evenodd" clip-rule="evenodd" d="M20.8487 1.43422C21.1611 1.74664 21.1611 2.25317 20.8487 2.56559L2.56584 20.8484C2.25342 21.1609 1.74689 21.1609 1.43447 20.8484L1.15162 20.5656C0.839204 20.2532 0.839202 19.7466 1.15162 19.4342L19.4345 1.15138C19.7469 0.838959 20.2534 0.838959 20.5658 1.15138L20.8487 1.43422Z" fill="white"/>\n' +
  '      </svg>\n' +
  '    </div>\n' +
  '\n' +
  '    <div class="js-gallery-photos gallery__photo">\n' +
  '      <img class="is-visible js-gallery-photo js-gallery-photo-1" src="img/gallery/2/1.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-2" src="img/gallery/2/2.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-3" src="img/gallery/2/3.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-4" src="img/gallery/2/4.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-5" src="img/gallery/2/5.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-6" src="img/gallery/2/6.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-7" src="img/gallery/2/7.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-8" src="img/gallery/2/8.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-9" src="img/gallery/2/9.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-10" src="img/gallery/2/10.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-11" src="img/gallery/2/11.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-12" src="img/gallery/2/12.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-13" src="img/gallery/2/13.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-14" src="img/gallery/2/15.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-15" src="img/gallery/2/16.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-16" src="img/gallery/2/18.jpg" alt="">\n' +
  '    </div>\n' +
  '\n' +
  '    <div class="js-gallery-prev gallery__prev img-w">\n' +
  '      <svg viewBox="0 0 32 12" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
  '        <path fill-rule="evenodd" clip-rule="evenodd" d="M6.77187 0.684955C6.83602 0.49252 6.60599 0.337848 6.45198 0.469858L0.354527 5.69624C0.168283 5.85588 0.168282 6.14401 0.354527 6.30365L6.45198 11.53C6.60599 11.662 6.83602 11.5074 6.77187 11.3149L5.16689 6.5H31.6C31.8209 6.5 32 6.32091 32 6.1V5.9C32 5.67909 31.8209 5.5 31.6 5.5H5.16686L6.77187 0.684955Z" fill="white"/>\n' +
  '      </svg>\n' +
  '    </div>\n' +
  '\n' +
  '    <div class="js-gallery-next gallery__next img-w">\n' +
  '      <svg viewBox="0 0 32 12" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
  '        <path fill-rule="evenodd" clip-rule="evenodd" d="M25.2288 11.3149C25.1647 11.5074 25.3947 11.662 25.5487 11.53L31.6461 6.30365C31.8324 6.14401 31.8324 5.85588 31.6461 5.69624L25.5487 0.469858C25.3947 0.337848 25.1647 0.49252 25.2288 0.684955L26.8338 5.5H0.4C0.179086 5.5 0 5.67909 0 5.9V6.1C0 6.32091 0.179086 6.5 0.4 6.5H26.8338L25.2288 11.3149Z" fill="white"/>\n' +
  '      </svg>\n' +
  '    </div>\n' +
  '\n' +
  '    <div class="gallery__info">\n' +
  '      <div class="gallery__title">Мероприятия</div>\n' +
  '      <div class="gallery__pagination">\n' +
  '        <span class="js-gallery-current">1</span> / <span class="js-gallery-amount">16</span>\n' +
  '      </div>\n' +
  '    </div>\n' +
  '  </div>\n' +
  '\n' +
  '  <div class="js-gallery js-gallery-3 gallery">\n' +
  '    <div class="js-gallery-close gallery__close" data-gallery="3">\n' +
  '      <svg viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
  '        <path fill-rule="evenodd" clip-rule="evenodd" d="M1.43447 1.15138C1.74689 0.838959 2.25342 0.838958 2.56583 1.15138L20.8487 19.4342C21.1611 19.7466 21.1611 20.2532 20.8487 20.5656L20.5658 20.8484C20.2534 21.1609 19.7469 21.1609 19.4345 20.8484L1.15162 2.56559C0.839204 2.25317 0.839203 1.74664 1.15162 1.43422L1.43447 1.15138Z" fill="white"/>\n' +
  '        <path fill-rule="evenodd" clip-rule="evenodd" d="M20.8487 1.43422C21.1611 1.74664 21.1611 2.25317 20.8487 2.56559L2.56584 20.8484C2.25342 21.1609 1.74689 21.1609 1.43447 20.8484L1.15162 20.5656C0.839204 20.2532 0.839202 19.7466 1.15162 19.4342L19.4345 1.15138C19.7469 0.838959 20.2534 0.838959 20.5658 1.15138L20.8487 1.43422Z" fill="white"/>\n' +
  '      </svg>\n' +
  '    </div>\n' +
  '\n' +
  '    <div class="js-gallery-photos gallery__photo">\n' +
  '      <img class="is-visible js-gallery-photo js-gallery-photo-1" src="img/gallery/3/1.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-2" src="img/gallery/3/2.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-3" src="img/gallery/3/3.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-4" src="img/gallery/3/4.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-5" src="img/gallery/3/5.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-6" src="img/gallery/3/6.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-7" src="img/gallery/3/7.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-8" src="img/gallery/3/8.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-9" src="img/gallery/3/9.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-10" src="img/gallery/3/10.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-11" src="img/gallery/3/11.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-12" src="img/gallery/3/12.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-13" src="img/gallery/3/13.jpg" alt="">\n' +
  '      <img class="js-gallery-photo js-gallery-photo-14" src="img/gallery/3/14.jpg" alt="">\n' +
  '    </div>\n' +
  '\n' +
  '    <div class="js-gallery-prev gallery__prev img-w">\n' +
  '      <svg viewBox="0 0 32 12" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
  '        <path fill-rule="evenodd" clip-rule="evenodd" d="M6.77187 0.684955C6.83602 0.49252 6.60599 0.337848 6.45198 0.469858L0.354527 5.69624C0.168283 5.85588 0.168282 6.14401 0.354527 6.30365L6.45198 11.53C6.60599 11.662 6.83602 11.5074 6.77187 11.3149L5.16689 6.5H31.6C31.8209 6.5 32 6.32091 32 6.1V5.9C32 5.67909 31.8209 5.5 31.6 5.5H5.16686L6.77187 0.684955Z" fill="white"/>\n' +
  '      </svg>\n' +
  '    </div>\n' +
  '\n' +
  '    <div class="js-gallery-next gallery__next img-w">\n' +
  '      <svg viewBox="0 0 32 12" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
  '        <path fill-rule="evenodd" clip-rule="evenodd" d="M25.2288 11.3149C25.1647 11.5074 25.3947 11.662 25.5487 11.53L31.6461 6.30365C31.8324 6.14401 31.8324 5.85588 31.6461 5.69624L25.5487 0.469858C25.3947 0.337848 25.1647 0.49252 25.2288 0.684955L26.8338 5.5H0.4C0.179086 5.5 0 5.67909 0 5.9V6.1C0 6.32091 0.179086 6.5 0.4 6.5H26.8338L25.2288 11.3149Z" fill="white"/>\n' +
  '      </svg>\n' +
  '    </div>\n' +
  '\n' +
  '    <div class="gallery__info">\n' +
  '      <div class="gallery__title">Офисы и заводы</div>\n' +
  '      <div class="gallery__pagination">\n' +
  '        <span class="js-gallery-current">1</span> / <span class="js-gallery-amount">14</span>\n' +
  '      </div>\n' +
  '    </div>\n' +
  '  </div>'

let isGallerySet = false

$(window).scroll(function() {
  const top_of_screen = $(window).scrollTop();

  if (top_of_screen > $(window).innerHeight() && !isGallerySet) {
    createGallery()
  }
});

function createGallery() {
  isGallerySet = true
  $('.js-gallery-content-box').html(gallery_content);
  setGalleryEvents();
}

function setGalleryEvents() {
  const $gallery = $('.js-gallery');
  const $showGalleryButton = $('.js-show-gallery');
  const $close = $('.js-gallery-close');
  const $prev = $('.js-gallery-prev');
  const $next = $('.js-gallery-next');
  let currentPhoto = 1;

  $showGalleryButton.click(function() {
    const galleryNum = $(this).data('gallery');

    $('body').addClass('is-locked');

    currentPhoto = 1;
    $('.js-gallery-photo').removeClass('is-visible');
    $('.js-gallery-photo-1').addClass('is-visible');
    $('.js-gallery-current').html(currentPhoto);

    $('.js-gallery-' + galleryNum).fadeIn(300);
  });

  $close.click(function() {
    const galleryNum = $(this).data('gallery');
    $('.js-gallery-' + galleryNum).fadeOut(300);
    $('body').removeClass('is-locked');
  });

  $prev.click(function() {
    const $container = $(this).closest('.js-gallery');
    const photos = $('.js-gallery-photos', $container).find('img');

    currentPhoto -= 1
    if (currentPhoto == 0) {
      currentPhoto = photos.length
    }

    $('.js-gallery-current', $container).html(currentPhoto);
    photos.removeClass('is-visible');
    $('.js-gallery-photo-' + currentPhoto, $container).addClass('is-visible');
  });

  $next.click(function() {
    const $container = $(this).closest('.js-gallery');
    const photos = $('.js-gallery-photos', $container).find('img');

    if (currentPhoto == photos.length) {
      currentPhoto = 0
    }
    currentPhoto += 1

    $('.js-gallery-current', $container).html(currentPhoto);
    photos.removeClass('is-visible');
    $('.js-gallery-photo-' + currentPhoto, $container).addClass('is-visible');
  });
}


$(document).ready(function() {
  if ($(window).scrollTop() > $(window).innerHeight()) {
    createGallery()
  }
});
