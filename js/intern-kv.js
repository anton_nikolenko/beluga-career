$(document).ready(function(){

  $('.js-intern-kv').addClass('is-animated');
  $('.js-intern-kv-bg').addClass('is-animated');

  $('.tooltip').hover(function() {
    $(this).addClass('is-visible');
  }, function() {
    $(this).removeClass('is-visible');
  });

  if (window.innerWidth <= 768) {
    $('.js-intern-kv').height(window.innerHeight);
  }


  $(window).scroll(function() {
    const $kvBlock = $('.js-intern-2nd');

    const top_of_element = $kvBlock.offset().top;
    const bottom_of_element = $kvBlock.offset().top + $kvBlock.outerHeight();
    const bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
    const top_of_screen = $(window).scrollTop();
    let secondRowOffset = 450

    if (window.innerWidth <= 768) {
      secondRowOffset = 250
    }

    if ((bottom_of_screen > top_of_element + 250) && (top_of_screen < bottom_of_element)) {
      $('.js-kv-list').addClass('is-visible');
    } else {
      $('.js-kv-list').removeClass('is-visible');
    }

    if ((bottom_of_screen > top_of_element + secondRowOffset) && (top_of_screen < bottom_of_element)) {
      $('.js-kv-list2').addClass('is-visible');
    } else {
      $('.js-kv-list2').removeClass('is-visible');
    }

  });


});
