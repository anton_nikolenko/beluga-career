$(document).ready(function(){
  $(window).scroll(function() {
    const $kvBlock = $('.js-kv-content');
    const $kvFadeBlock = $('.js-kv-fade-block');
    /*const $block = $('.js-career-cases');
    const top_of_element = $block.offset().top;
    const bottom_of_element = $block.offset().top + $block.outerHeight();
    const bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();*/
    const top_of_screen = $(window).scrollTop();

    if (top_of_screen >= 30) {
      $kvBlock.addClass('is-out')

      let opercent = (($(window).innerHeight()/100) / top_of_screen)*10;

      let fadepercent = ((top_of_screen/($(window).innerHeight()/100))/100) + 0.2;

      if (opercent < 0.25) opercent = 0.25

      $kvBlock.css('opacity', opercent)
      $kvFadeBlock.css('opacity', fadepercent)
    } else {
      $kvBlock.css('opacity', 1)
      $kvBlock.removeClass('is-out')
    }



  });


});
