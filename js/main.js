function changeMainTab(mainCurrentTab) {
  const $mainTab = $('.js-main-tab');
  const $mainImg = $('.js-main-img');
  const animDuration = 250;
  const $mainTabBtn = $('.js-main-tab-btn');

  if (mainCurrentTab == 5 && window.innerWidth >= 768) {
    $('.js-main-box').addClass('is-660')
  } else {
    $('.js-main-box').removeClass('is-660')
  }

  if (mainCurrentTab == 6) {
    $('.js-main-box').addClass('is-660')
  } else {
    $('.js-main-box').removeClass('is-660')
  }

  $mainTabBtn.closest('li').removeClass('is-active');
  $('.js-main-tab-btn-' + mainCurrentTab).addClass('is-active');

  $mainTab.removeClass('is-active');
  setTimeout(() => {
    $('.js-main-tab-' + mainCurrentTab).addClass('is-active');
  }, animDuration)

  $mainImg.removeClass('is-active');
  setTimeout(() => {
    $('.js-main-img-' + mainCurrentTab).addClass('is-active');
  }, animDuration)

  $('.js-main-tabline').removeClass('is-active');
  $('.js-main-tabline-' + mainCurrentTab).addClass('is-active');
}

function initTagsScroll(ele) {
  ele.style.cursor = 'grab';

  let pos = { top: 0, left: 0, x: 0, y: 0 };

  const mouseDownHandler = function (e) {
    ele.style.cursor = 'grabbing';
    ele.style.userSelect = 'none';

    pos = {
      left: ele.scrollLeft,
      top: ele.scrollTop,
      x: e.clientX,
      y: e.clientY,
    };

    document.addEventListener('mousemove', mouseMoveHandler);
    document.addEventListener('mouseup', mouseUpHandler);
  };

  const mouseMoveHandler = function (e) {
    const dx = e.clientX - pos.x;
    const dy = e.clientY - pos.y;

    ele.scrollTop = pos.top - dy;
    ele.scrollLeft = pos.left - dx;
  };

  const mouseUpHandler = function () {
    ele.style.cursor = 'grab';
    ele.style.removeProperty('user-select');

    document.removeEventListener('mousemove', mouseMoveHandler);
    document.removeEventListener('mouseup', mouseUpHandler);
  };

  ele.addEventListener('mousedown', mouseDownHandler);
}

$(document).ready(function() {
  const $ele = $('.js-tags-container');

  $ele.each(function() {
    initTagsScroll($(this)[0])
  });

  const $mainTabBtn = $('.js-main-tab-btn');
  let mainCurrentTab = 1;
  const $mainBox = $('.js-main-box');


  $('.js-main-pause').on('mousedown touchstart', function () {
    $('.js-main-tabline').addClass('is-paused')
  });
  $('.js-main-pause').on('mouseup touchend', function () {
    $('.js-main-tabline').removeClass('is-paused')
  });


  $(window).scroll(function() {
    const top_of_element = $mainBox.offset().top;
    const bottom_of_element = $mainBox.offset().top + $mainBox.outerHeight();
    const bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
    const top_of_screen = $(window).scrollTop();


    if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)) {
      $mainBox.addClass('is-visible');
    } else {
      $mainBox.removeClass('is-visible');
    }
  });

  $('.js-career-switch1').click(function() {
    changeMainTab('2')
  });

  $('.js-career-switch2').click(function() {
    changeMainTab('1')
  });

  $('.js-career-switch3').click(function() {
    changeMainTab('4')
  });

  $('.js-career-switch4').click(function() {
    changeMainTab('3')
  });

  $mainTabBtn.click(function() {
    //if ($(this).closest('li').hasClass('is-active')) return false

    mainCurrentTab = $(this).data('main');

    changeMainTab(mainCurrentTab)
  });

  $('.js-animation-trigger').on('animationend', function() {
    if (mainCurrentTab < 6) {
      mainCurrentTab += 1
    } else {
      mainCurrentTab = 1
    }

    changeMainTab(mainCurrentTab)
  });

});
