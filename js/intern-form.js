function checkApplyForm() {
  let $formInput = $('.js-apply-form-input ');
  let checkbox = $('.js-apply-form-agreement');

  $formInput.each(function() {
    let item = $(this)

    if (!item.val()) {
      item.closest('.js-apply-form-input-box').addClass('is-error');
      $('.js-apply-form-error-text').addClass('is-visible');
    }
  });

  if (!checkbox.is(':checked')) {
    checkbox.closest('.js-apply-form-input-box').addClass('is-error');
    $('.js-apply-form-error-text').addClass('is-visible');
  }

  const CVfile = $("#apply-form-cv")[0].files[0]
  const portfolioFile = $("#apply-form-portfolio")[0].files[0]

  if (CVfile) {
    if (CVfile.size > 31457280) {
      $('.js-apply-form-cv-box').addClass('is-error')
      $('.js-apply-form-error-size-text').addClass('is-visible');
    }
  }

  if (portfolioFile) {
    if (portfolioFile.size > 31457280) {
      $('.js-apply-form-portfolio-box').addClass('is-error');
      $('.js-apply-form-error-size-text').addClass('is-visible');
    }
  }


  if ($('.is-error').length) {
    return false
  } else {
    return true
  }
}

function clearApplyFormErrors() {
  $('.js-apply-form-error-text').removeClass('is-visible');
  $('.js-apply-form-input-box').removeClass('is-error');
  $('.js-apply-form-cv-box').removeClass('is-error');
  $('.js-apply-form-portfolio-box').removeClass('is-error');
  $('.js-apply-form-error-size-text').removeClass('is-visible');
}

$(document).ready(function(){
  const $applyForm = $('.js-apply-form')
  const $showFormBtn = $('.js-show-apply-form')

  $showFormBtn.click(function() {
    let select = $(this).data('select');
    if (select) {
      $('.js-apply-form-select').val(select)
    } else {
      $('.js-apply-form-select').val(0)
    }
    $applyForm.fadeIn(300)
    $('body').addClass('is-modal-opened')
  });

  $('.js-apply-form-close').click(function() {
    $applyForm.fadeOut(300)
    $('body').removeClass('is-modal-opened')
  });

  $('.js-apply-form-input').change(function(){
    clearApplyFormErrors()
  });

  $('.js-apply-form-input-nr').change(function(){
    clearApplyFormErrors()
  });

  $('.js-apply-form-send').click(function(e){
    e.preventDefault();

    if (checkApplyForm()) {
      $applyForm.fadeOut(300, function() {
        $('.js-apply-form-success').fadeIn(300)
      });
    } else {
      // ...
    }
  });

  $('.js-apply-form-success-close').click(function() {
    $('.js-apply-form-success').fadeOut(300);
  });
});
