$(document).ready(function(){
  $(window).scroll(function() {
    const $joinBlock = $('.js-intern-join');

    const top_of_element = $joinBlock.offset().top;
    const bottom_of_element = $joinBlock.offset().top + $joinBlock.outerHeight();
    const bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
    const top_of_screen = $(window).scrollTop();

    const visOffsetStart = 350;


    if ((bottom_of_screen > top_of_element + visOffsetStart) && (top_of_screen < bottom_of_element)) {
      $joinBlock.addClass('is-visible')
    } else {
      $joinBlock.removeClass('is-visible')
    }
    if ((bottom_of_screen > top_of_element + 500) && (top_of_screen < bottom_of_element)) {
      $joinBlock.addClass('is-visible-2')
    } else {
      $joinBlock.removeClass('is-visible-2')
    }

  });


});
