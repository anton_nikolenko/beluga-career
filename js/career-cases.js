$(document).ready(function(){
  $(window).scroll(function() {
    const $block = $('.js-career-cases');
    const top_of_element = $block.offset().top;
    const bottom_of_element = $block.offset().top + $block.outerHeight();

    const bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
    const top_of_screen = $(window).scrollTop();
    const $title = $('.js-career-cases-title');
    const top_of_last = $('.js-main-last-el').offset().top;

    const $item1 = $('.js-career-cases-item1');
    const top_of_item1 = $item1.offset().top;
    const bottom_of_item1 = $item1.offset().top + $item1.outerHeight();

    const $item2 = $('.js-career-cases-item2');
    const top_of_item2 = $item2.offset().top;
    const bottom_of_item2 = $item2.offset().top + $item2.outerHeight();

    const $item3 = $('.js-career-cases-item3');
    const top_of_item3 = $item3.offset().top;
    const bottom_of_item3 = $item3.offset().top + $item3.outerHeight();

    const $item4 = $('.js-career-cases-item4');
    const top_of_item4 = $item4.offset().top;
    const bottom_of_item4 = $item4.offset().top + $item4.outerHeight();


    if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)) {

    } else {
      // console.log('nope')
    }


    const visOffsetStart = $(window).innerHeight()/100*40;
    const visOffsetEnd = $(window).innerHeight()/100*40;

    if ((bottom_of_screen > top_of_element + $(window).innerHeight()/100*60) && (top_of_screen < top_of_item1 - $(window).innerHeight()/100*60)) {
      if (window.innerWidth > 768) {
        $title.addClass('is-visible');
      }
    } else {
      if (window.innerWidth > 768) {
        $title.removeClass('is-visible');
      }
    }

    if ((bottom_of_screen > top_of_element + 250) && (top_of_screen < bottom_of_element)) {
      if (window.innerWidth <= 768) {
        $title.addClass('is-visible');
        $block.addClass('is-mobile-animated')
      }
    } else {
      if (window.innerWidth <= 768) {
        $title.removeClass('is-visible');
        $block.removeClass('is-mobile-animated');
      }
    }

    if (bottom_of_screen > (top_of_item1 + visOffsetStart) && top_of_screen < bottom_of_item1 - visOffsetEnd) {
      $item1.addClass('is-visible');
    } else {
      $item1.removeClass('is-visible');
    }

    if (bottom_of_screen > (top_of_item2 + visOffsetStart) && top_of_screen < bottom_of_item2 - visOffsetEnd) {
      $item2.addClass('is-visible');
    } else {
      $item2.removeClass('is-visible');
    }

    if (bottom_of_screen > (top_of_item3 + visOffsetStart) && top_of_screen < bottom_of_item3 - visOffsetEnd) {
      $item3.addClass('is-visible');
    } else {
      $item3.removeClass('is-visible');
    }

    if (bottom_of_screen > (top_of_item4 + visOffsetStart) && top_of_screen < bottom_of_item4 - visOffsetEnd) {
      $item4.addClass('is-visible');
    } else {
      $item4.removeClass('is-visible');
    }

    if (top_of_screen >= top_of_last - $(window).innerHeight()/100*15) {
      $item4.addClass('is-fixed')
    } else {
      $item4.removeClass('is-fixed')
    }

    const top_of_main = $('.js-main-box').offset().top

    if (bottom_of_screen >= (top_of_main + 100)) {

      let perc = $(window).innerHeight()/100

      let opct = 0.8 - ((bottom_of_screen - top_of_main)/perc)/100

      if (opct <= 0) opct = 0

      $item4.css('opacity', opct)
      $('.js-career-cases-out-box').addClass('is-out')
    } else {
      $item4.css('opacity', 1)
      $('.js-career-cases-out-box').removeClass('is-out')
    }
  });


});
