function changeTab(aboutCurrentTab) {
  const $aboutTab = $('.js-about-tab');
  const $aboutImg = $('.js-about-img');
  const animDuration = 250;
  const $aboutTabBtn = $('.js-about-tab-btn');

  $aboutTabBtn.removeClass('is-active');
  $('.js-about-tab-btn-' + aboutCurrentTab).addClass('is-active');

  $aboutTab.removeClass('is-active');
  setTimeout(() => {
    $('.js-about-tab-' + aboutCurrentTab).addClass('is-active');
  }, animDuration)

  $aboutImg.removeClass('is-active');
  setTimeout(() => {
    $('.js-about-img-' + aboutCurrentTab).addClass('is-active');
  }, animDuration)
}

$(document).ready(function() {
  const $careerAboutBlock = $('.js-career-about');
  const $aboutTabBtn = $('.js-about-tab-btn');
  let aboutCurrentTab = 1;

  $(window).scroll(function() {
    const top_of_element = $careerAboutBlock.offset().top;
    const bottom_of_element = $careerAboutBlock.offset().top + $careerAboutBlock.outerHeight();
    const bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
    const top_of_screen = $(window).scrollTop();

    if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)) {
      $careerAboutBlock.addClass('is-visible');
    } else {
      $careerAboutBlock.removeClass('is-visible');
    }
  });


  $('.js-career-about-pause').on('mousedown touchstart', function () {
    $('.js-about-tab-btn').addClass('is-paused')
  });
  $('.js-career-about-pause').on('mouseup touchend', function () {
    $('.js-about-tab-btn').removeClass('is-paused')
  });


  $aboutTabBtn.click(function() {
    aboutCurrentTab = $(this).data('about');

    changeTab(aboutCurrentTab)
  });

  $('.js-about-tab-btn').on('animationend', function() {
    if (aboutCurrentTab < 5) {
      aboutCurrentTab += 1
    } else {
      aboutCurrentTab = 1
    }

    changeTab(aboutCurrentTab)
  });


});
