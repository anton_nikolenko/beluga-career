$(document).ready(function(){
  $(window).scroll(function() {
    const $ctaBlock = $('.js-intern-cta');
    const top_of_element = $ctaBlock.offset().top;

    const bottom_of_element = $ctaBlock.offset().top + $ctaBlock.outerHeight();
    const bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
    const top_of_screen = $(window).scrollTop();

    const visOffsetStart = 350;


    if ((bottom_of_screen > top_of_element + visOffsetStart) && (top_of_screen < bottom_of_element)) {
      $ctaBlock.addClass('is-visible')
    } else {
      $ctaBlock.removeClass('is-visible')
    }

  });


});
