$(document).ready(function(){
  $(window).scroll(function() {
    const $eduBlock = $('.js-intern-way');

    const top_of_element = $eduBlock.offset().top;
    const bottom_of_element = $eduBlock.offset().top + $eduBlock.outerHeight();
    const bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
    const top_of_screen = $(window).scrollTop();

    const visOffsetStart = 350;


    if ((bottom_of_screen > top_of_element + visOffsetStart) && (top_of_screen < bottom_of_element)) {
      $eduBlock.addClass('is-visible')
    } else {
      $eduBlock.removeClass('is-visible')
    }

  });


});
