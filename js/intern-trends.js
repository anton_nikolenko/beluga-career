$(document).ready(function(){
  $(window).scroll(function() {
    const $trendsBlock = $('.js-intern-trends');

    const top_of_element = $trendsBlock.offset().top;
    const bottom_of_element = $trendsBlock.offset().top + $trendsBlock.outerHeight();
    const bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
    const top_of_screen = $(window).scrollTop();

    const visOffsetStart = 250;


    if ((bottom_of_screen > top_of_element + visOffsetStart) && (top_of_screen < bottom_of_element)) {
      $trendsBlock.addClass('is-visible')
    } else {
      $trendsBlock.removeClass('is-visible')
    }

  });


});
