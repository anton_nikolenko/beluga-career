$(document).ready(function(){
  $(window).scroll(function() {
    const $benBlock = $('.js-ben-block');

    const top_of_element = $benBlock.offset().top;
    const bottom_of_element = $benBlock.offset().top + $benBlock.outerHeight();
    const bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
    const top_of_screen = $(window).scrollTop();

    const visOffsetStart = $(window).innerHeight()/100*40;

    if ((bottom_of_screen > top_of_element + visOffsetStart) && (top_of_screen < bottom_of_element)) {
      $benBlock.addClass('is-visible')
    } else {
      $benBlock.removeClass('is-visible')
    }

  });


});
